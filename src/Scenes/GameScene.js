/*
* Connects the client side with socket.io to the server-side and displays the data to the user.
*/

import 'phaser';
import screen01 from '../Helpers/Screens/screen01';
import GamePlay from '../Helpers/GamePlay/GamePlay';

export default class GameScene extends Phaser.Scene {


  constructor() {
    super({ key: 'game' });

    this.screenNumber = 0;
    
    this.playerName = '';
    this.email = '';
    this.phoneNumber = '';
    this.scale = '0';
    this.isGameCreated = false;
  }

  preload() {
    this.width = this.sys.game.canvas.width;
    this.height = this.sys.game.canvas.height;
    this.centerX = this.width / 2;
    this.centerY = this.height / 2;
  }

  create() {


    this.screen01 = new screen01({
      scene: this,
      x: 0,
      y: 0,
      changeScreenCallBack: this.ChangeScreen,
      parent: this
    });
    this.add.existing(this.screen01);
    this.gamePlay = new GamePlay({
      scene: this,
      x: 0,
      y: 0,
      parent:this
    });
    this.add.existing(this.gamePlay);
    this.ChangeScreen();
  }


  ConfirmPrivacy(playerName, email){
    this.playerName = playerName;
    this.email = email;
    this.ChangeScreen();
  }

  PrevScreen() {
    this.screenNumber = this.screenNumber-2;
    this.ChangeScreen();
  }

  ScaleSelect(scale){
    this.scale = scale;
    this.ChangeScreen();
  }

  ChangeScreen() {
    console.log(this.screenNumber);
     this.screen01.visible = false;
     this.gamePlay.visible = false;

     this.screen01.active = false;
     this.gamePlay.active = false;

    let resetGame = false;
    switch (this.screenNumber) {
      case 0:
         // this.SkipScreen();
          this.screen01.visible = true;
          this.screen01.active = true;
        break;
      case 1:
          this.gamePlay.visible = true;
          this.gamePlay.active = true;
          this.gamePlay.addPhone();
        break;

      default:
        break;
    }

      if(!resetGame) {
        this.screenNumber++;
      }else{
          this.resetGame();
      }
    }

    ResetGame(){
        this.gamePlay.destroy();

        this.gamePlay = new GamePlay({
        scene: this,
        x: 0,
        y: 0,
        backgroundImage: 'gamePlay'
        });
        this.add.existing(this.gamePlay);

        this.screenNumber = 3;
        //this.gamePlay = 0;
        // this.avatar = "red_avatar";
        // this.playerName = '';
        // this.postCode = '';
        // this.phoneNumber = '';
        // this.scale = '0';
        //this.ChangeScreen();
        this.gamePlay.ResetGame();
        // this.screen09.visible = false;
        // this.screen03.visible = true;
        // this.screen03.active = true;
    }

    PlayMusic(){
        this.music.play();
    }

    PauseMusic(){
        this.music.pause();
    }

    SendData() {
        window.sendToServer(this.playerName, this.email);
    }

}
