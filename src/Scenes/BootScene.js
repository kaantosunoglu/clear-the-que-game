

class BootScene extends Phaser.Scene {
    constructor(test) {
        super({
            key: 'BootScene'
        });

        this.control = 0;

    }

  preload () {

    this.width = this.sys.game.canvas.width;
    this.height = this.sys.game.canvas.height;
    this.centerX = this.width / 2;
    this.centerY = this.height / 2;

    // window.PlayVideo1();

    //VIDEO
    //this.load.video('video1', 'assets/video/ProximityEmbSignatureVideoRRP.mp4', 'loadeddata', true, false);
    // this.load.video('video2', 'assets/video/EmbassyInteractiveGame.mp4', 'loadeddata', true, false);


   
    // this.load.image('holdImage', 'assets/images/Screens/holdingpage.png');

      //SOUNDS

      //IMAGES
    this.load.image('screen01_back', 'assets/images/screens/screen_01.png');  
    this.load.image('game_back', 'assets/images/screens/game_back.png');  
    this.load.image('entertheshop', 'assets/images/entertheshop.png');  
    this.load.image('redeemcode', 'assets/images/redeemcode.png');  
    this.load.image('playagain', 'assets/images/playagain.png');  
    this.load.image('desk', 'assets/images/screens/desk.png');  
    this.load.image('bubble', 'assets/images/screens/bubble.png');
    this.load.image('bubble2', 'assets/images/screens/bubble2.png');  
    this.load.image('correct', 'assets/images/screens/correct.png');  
    this.load.image('wrong', 'assets/images/screens/wrong.png');  

    //PHONE
    this.load.image('phone', 'assets/images/screens/phone.png');  
    this.load.image('hp01', 'assets/images/howto/hp01.png');  
    this.load.image('hp02', 'assets/images/howto/hp02.png');  
    this.load.image('hp03', 'assets/images/howto/hp03.png');  
    this.load.image('hp04', 'assets/images/howto/hp04.png');  
    this.load.image('hp05', 'assets/images/howto/hp05.png');  
    this.load.image('hp06', 'assets/images/howto/hp06.png');  

    //BRANDS
    this.load.image('amberleaf', 'assets/images/brandlogos/amberleaf.png');
    this.load.image('drum', 'assets/images/brandlogos/drum.png');
    this.load.image('goldenvirginiatheoriginal', 'assets/images/brandlogos/goldenvirginiatheoriginal.png');
    this.load.image('goldenvirginiayellow', 'assets/images/brandlogos/goldenvirginiayellow.png');
    this.load.image('goldleaf', 'assets/images/brandlogos/goldleaf.png');
    this.load.image('players', 'assets/images/brandlogos/players.png');
    this.load.image('sterling', 'assets/images/brandlogos/sterling.png');
    this.load.image('jsp', 'assets/images/brandlogos/jsp.png');


    //ANIMATIONS
    
      //Checkout Lady
    this.load.image('checkoutlady_01', 'assets/images/animations/Checkout Lady/Till Lady 1.png');  
    this.load.image('checkoutlady_02', 'assets/images/animations/Checkout Lady/Till Lady 2.png');  
    this.load.image('checkoutlady_03', 'assets/images/animations/Checkout Lady/Till Lady 3.png');  
    this.load.image('checkoutlady_04', 'assets/images/animations/Checkout Lady/Till Lady 4.png');  
    this.load.image('checkoutlady_05', 'assets/images/animations/Checkout Lady/Till Lady 5.png');  
    this.load.image('checkoutlady_06', 'assets/images/animations/Checkout Lady/Till Lady 6.png');  
      //Blue Shirt
    this.load.image('blueshirt_01', 'assets/images/animations/Man in blue shirt/man in blue shirt 1.png');    
    this.load.image('blueshirt_02', 'assets/images/animations/Man in blue shirt/man in blue shirt 2.png');    
    this.load.image('blueshirt_03', 'assets/images/animations/Man in blue shirt/man in blue shirt 3.png');    
    this.load.image('blueshirt_04', 'assets/images/animations/Man in blue shirt/man in blue shirt 4.png');    
    this.load.image('blueshirt_05', 'assets/images/animations/Man in blue shirt/man in blue shirt 5.png');    
    this.load.image('blueshirt_06', 'assets/images/animations/Man in blue shirt/man in blue shirt 6.png');    
    this.load.image('blueshirt_07', 'assets/images/animations/Man in blue shirt/man in blue shirt 7.png');    
    this.load.image('blueshirt_08', 'assets/images/animations/Man in blue shirt/man in blue shirt 8.png');    
    this.load.image('blueshirt_09', 'assets/images/animations/Man in blue shirt/man in blue shirt 9.png');    
    this.load.image('blueshirt_10', 'assets/images/animations/Man in blue shirt/man in blue shirt 10.png');    
      //Old Lady
    this.load.image('oldlady_01', 'assets/images/animations/Older Yellow lady/Women in yellow still 1.png');  
    this.load.image('oldlady_02', 'assets/images/animations/Older Yellow lady/Women in yellow still 2.png');  
    this.load.image('oldlady_03', 'assets/images/animations/Older Yellow lady/Women in yellow still 3.png');  
    this.load.image('oldlady_04', 'assets/images/animations/Older Yellow lady/Women in yellow still 4.png');  
    this.load.image('oldlady_05', 'assets/images/animations/Older Yellow lady/Women in yellow still 5.png');  
    this.load.image('oldlady_06', 'assets/images/animations/Older Yellow lady/Women in yellow still 6.png');  
    this.load.image('oldlady_07', 'assets/images/animations/Older Yellow lady/Women in yellow still 7.png');  
    this.load.image('oldlady_08', 'assets/images/animations/Older Yellow lady/Women in yellow still 8.png');  
    this.load.image('oldlady_09', 'assets/images/animations/Older Yellow lady/Women in yellow still 9.png');  
    this.load.image('oldlady_10', 'assets/images/animations/Older Yellow lady/Women in yellow still  with blinked eye 10.png');  
      //Pink Woman
    this.load.image('pinkwoman_01', 'assets/images/animations/Pink Women/Women in pink dress 1.png');
    this.load.image('pinkwoman_02', 'assets/images/animations/Pink Women/Women in pink dress 2.png');
    this.load.image('pinkwoman_03', 'assets/images/animations/Pink Women/Women in pink dress 3.png');
    this.load.image('pinkwoman_04', 'assets/images/animations/Pink Women/Women in pink dress 4.png');
    this.load.image('pinkwoman_05', 'assets/images/animations/Pink Women/Women in pink dress 5.png');
    this.load.image('pinkwoman_06', 'assets/images/animations/Pink Women/Women in pink dress 6.png');
    this.load.image('pinkwoman_07', 'assets/images/animations/Pink Women/Women in pink dress 7.png');
    this.load.image('pinkwoman_08', 'assets/images/animations/Pink Women/Women in pink dress 8.png');
    this.load.image('pinkwoman_09', 'assets/images/animations/Pink Women/Women in pink dress 9.png');  
    this.load.image('pinkwoman_10', 'assets/images/animations/Pink Women/Women in pink dress with blinking eye 10.png');
      //Yellow Woman
    this.load.image('yellowwoman_01', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 1.png');
    this.load.image('yellowwoman_02', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 2.png');
    this.load.image('yellowwoman_03', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 3.png');
    this.load.image('yellowwoman_04', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 4.png');
    this.load.image('yellowwoman_05', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 5.png');
    this.load.image('yellowwoman_06', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 6.png');
    this.load.image('yellowwoman_07', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 7.png');
    this.load.image('yellowwoman_08', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 8.png');
    this.load.image('yellowwoman_09', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 9.png');
    this.load.image('yellowwoman_10', 'assets/images/animations/Yellow lady 2/Women in yellow peplum 10.png');


    this.load.on('complete', () => {
      //this.loadingText.destroy();
      //this.progressBar.destroy();
      //this.playBtn = this.add.image(this.centerX, this.centerY, 'playBtn');
      // this.playBtn.setScale(0.4)
      // this.playBtn.setInteractive({ useHandCursor: true })
      // this.playBtn.name = 'play';

        this.sys.game.config.scaleMode = this.scale.orientation === Phaser.Scale.PORTRAIT ? Phaser.Scale.HEIGHT_CONTROLS_WIDTH : Phaser.Scale.HEIGHT_CONTROLS_WIDTH;
        this.scene.start('game');
      //this.changeScreen(null);
    });


  }

  create() {
    // window.player1.on('ended', function() {
    //     console.log('Played the video');
    //     window.StopVideo1();
    //    // this.changeScreen(null);
    //    this.control++;
    // }.bind(this));
  }

update() {

    if(window.finishedVideo1 && window.finishedVideo1 == true)
     {
        window.StopVideo1();

//window.resize();
        this.control++;
     }
    if(this.control >= 2){
        console.log(this.scale.orientation + "    "  +this.sys.game.config.scaleMode + "  ooooooo")
        this.sys.game.config.scaleMode = this.scale.orientation === Phaser.Scale.PORTRAIT ? Phaser.Scale.HEIGHT_CONTROLS_WIDTH : Phaser.Scale.HEIGHT_CONTROLS_WIDTH;
        this.scene.start('game');
       // this.scale.on('resize', this.resize, this);


}
}


  // handleClicks(pointer, object) {
    changeScreen(video) {

        this.control++;
        console.log(this.control)

                // this.holdImage = this.add.image(this.centerX, this.centerY, 'holdImage');
                // this.holdImage.setInteractive();

                // this.holdImage.on('pointerup', function () {
                //     this.holdImage.destroy();

                // }.bind(this));


                //this.orientation === Phaser.Scale.PORTRAIT ? Phaser.Scale.HEIGHT_CONTROLS_WIDTH : Phaser.Scale.HEIGHT_CONTROLS_WIDTH


            }
}

export default BootScene;
