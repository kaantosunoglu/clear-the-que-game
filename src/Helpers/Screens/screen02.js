export default class screen02 extends Phaser.GameObjects.Container {
    constructor(config){
        super(config.scene, config.x, config.y, config.children);

        this.scene = config.scene;

        this.create();
    }

    create() {
        this.setSize(980, 674);
        this.background = this.scene.add.sprite(0,0, 'screen01_back');
       // this.background.setScale(.5);
        this.add(this.background);
        this.setPosition(980/2, 674/2);

        this.enterButton = this.scene.add.sprite(290, 290, 'entertheshop');
        // this.enterButton.setScale(.5);
        this.enterButton.setInteractive();
        this.add(this.enterButton);

        this.enterButton.on('pointerup', function () {
 
        }.bind(this));

    }


}