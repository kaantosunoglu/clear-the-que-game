import phone from './phone';
import question from './question';
import endGame from './gameEnd';
import gameEnd from './gameEnd';

export default class gameplay extends Phaser.GameObjects.Container {
    constructor(config){
        super(config.scene, config.x, config.y, config.children);

        this.scene = config.scene;

        this.questionNumber = 1;

        this.success = false;

        this.create();
    }

    create() {
        this.setSize(980, 674);
        this.background = this.scene.add.sprite(0,0, 'game_back');
       // this.background.setScale(.5);
        
        this.setPosition(980/2, 674/2);

        this.desk = this.scene.add.sprite(0, 0, 'desk');


        this.footerText = this.scene.add.text(-370, 310, '*Based on RRP as at July 2021. For the avoidance of doubt, retailers are free at all times to determine the selling price of their products.', { fontFamily: 'raleway_regular', fontSize: '10pt', color: '#000000', align: 'left' }).setOrigin(0,0);
      
    
        this.scene.anims.create({
            key: 'lady',
            frames: [
                { key: 'checkoutlady_01', frame: null, duration: 650 },
                { key: 'checkoutlady_02', frame: null, duration: 50 },
                { key: 'checkoutlady_03', frame: null, duration: 650 },
                { key: 'checkoutlady_04', frame: null, duration: 50 },
                { key: 'checkoutlady_05', frame: null, duration: 650 },
                { key: 'checkoutlady_06', frame: null, duration: 650 }
            ],
            frameRate: 4,
            repeat: 1
        });


    
        this.lady = this.scene.add.sprite(-230, -122, 'checkoutlady_03');
        this.lady.setScale(.57)
       

        this.add([this.background, this.lady, this.desk]);
        this.createAnimations();
        this.createQuestions();

        this.endGame = new gameEnd({
            scene:this.scene,
            parent:this,
            endGameCallBack: this.handleEndGame
        });
        this.endGame.visible = false;
        this.scene.add.existing(this.endGame);
        this.add(this.endGame);
        this.add(this.footerText);
    }

    createAnimations() {

         //PINKWOMAN
         this.scene.anims.create({
            key: 'pinkwomanWalk',
            frames: [
                { key: 'pinkwoman_01', frame: null, duration: 50 },
                { key: 'pinkwoman_02', frame: null, duration: 50 },
                { key: 'pinkwoman_03', frame: null, duration: 50 },
                { key: 'pinkwoman_04', frame: null, duration: 50 },
                { key: 'pinkwoman_05', frame: null, duration: 50 },
                { key: 'pinkwoman_06', frame: null, duration: 50 },
                { key: 'pinkwoman_07', frame: null, duration: 50 },
                { key: 'pinkwoman_08', frame: null, duration: 50 }
            ],
            frameRate: 6,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'pinkwomanIdle',
            frames: [
                { key: 'pinkwoman_09', frame: null, duration: 4345 },
                { key: 'pinkwoman_10', frame: null, duration: 50 }
            ],
            frameRate: 4,
            repeat: -1
        });

        this.pinkwoman = this.scene.add.sprite(600,10, 'pinkwoman_09');
        this.pinkwoman.setScale(.64);
        this.pinkwoman.play('pinkwomanIdle');
        this.add(this.pinkwoman);

         //YELLOWWOMAN
         this.scene.anims.create({
            key: 'yellowwomanWalk',
            frames: [
                { key: 'yellowwoman_01', frame: null, duration: 50 },
                { key: 'yellowwoman_02', frame: null, duration: 50 },
                { key: 'yellowwoman_03', frame: null, duration: 50 },
                { key: 'yellowwoman_04', frame: null, duration: 50 },
                { key: 'yellowwoman_05', frame: null, duration: 50 },
                { key: 'yellowwoman_06', frame: null, duration: 50 },
                { key: 'yellowwoman_07', frame: null, duration: 50 },
                { key: 'yellowwoman_08', frame: null, duration: 50 }
            ],
            frameRate: 4,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'yellowwomanIdle',
            frames: [
                { key: 'yellowwoman_09', frame: null, duration: 4345 },
                { key: 'yellowwoman_10', frame: null, duration: 50 }
            ],
            frameRate: 4,
            repeat: -1
        });

        this.yellowwoman = this.scene.add.sprite(780, 10, 'yellowwoman_09');
        this.yellowwoman.setScale(.64);
        this.yellowwoman.play('yellowwomanIdle')
        this.add(this.yellowwoman);

        //BLUESHIRT
        this.scene.anims.create({
            key: 'blueshirtWalk',
            frames: [
                { key: 'blueshirt_01', frame: null, duration: 50 },
                { key: 'blueshirt_02', frame: null, duration: 50 },
                { key: 'blueshirt_03', frame: null, duration: 50 },
                { key: 'blueshirt_04', frame: null, duration: 50 },
                { key: 'blueshirt_05', frame: null, duration: 50 },
                { key: 'blueshirt_06', frame: null, duration: 50 },
                { key: 'blueshirt_07', frame: null, duration: 50 },
                { key: 'blueshirt_08', frame: null, duration: 50 }
            ],
            frameRate: 4,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'blueshirtIdle',
            frames: [
                { key: 'blueshirt_09', frame: null, duration: 4000 },
                { key: 'blueshirt_10', frame: null, duration: 50 }
            ],
            frameRate: 4,
            repeat: -1
        });

        this.blueshirt = this.scene.add.sprite(930, 0, 'blueshirt_09');
        this.blueshirt.setScale(.64);
        this.blueshirt.play('blueshirtIdle')
        this.add(this.blueshirt);

        //OLDLADY
        this.scene.anims.create({
            key: 'oldladyWalk',
            frames: [
                { key: 'oldlady_01', frame: null, duration: 50 },
                { key: 'oldlady_02', frame: null, duration: 50 },
                { key: 'oldlady_03', frame: null, duration: 50 },
                { key: 'oldlady_04', frame: null, duration: 50 },
                { key: 'oldlady_05', frame: null, duration: 50 },
                { key: 'oldlady_06', frame: null, duration: 50 },
                { key: 'oldlady_07', frame: null, duration: 50 },
                { key: 'oldlady_08', frame: null, duration: 50 }
            ],
            frameRate: 4,
            repeat: -1
        });

        this.scene.anims.create({
            key: 'oldladyIdle',
            frames: [
                { key: 'oldlady_09', frame: null, duration: 5321 },
                { key: 'oldlady_10', frame: null, duration: 50 }
            ],
            frameRate: 4,
            repeat: -1
        });

        this.oldlady = this.scene.add.sprite(1080,10, 'oldlady_09');
        this.oldlady.setScale(.64);
        this.oldlady.play('oldladyIdle')
        this.add(this.oldlady);
       
    }

    createQuestions(){
        this.questions = [
            {
                questionText:'What’s your lowest priced rolling tobacco?',
                answer1: 'amberleaf',
                answer2: 'sterling',
                answer3: 'players',
                fontSize: 24
            },
            {
                questionText:'I’m currently smoking cigarettes but I’m thinking about trying Rolling Tobacco, what do you recommend for a newcomer?',
                answer1: 'players',
                answer2: 'goldleaf',
                answer3: 'goldenvirginiayellow',
                fontSize: 16
            },
            {
                questionText:'Hi, I’m your local Imperial Tobacco rep.\nA quick test, do you know which brand gives 15% POR at the moment*? ',
                answer1: 'drum',
                answer2: 'players',
                answer3: 'goldenvirginiatheoriginal',
                fontSize: 16
            },
            {
                questionText:'I really struggle with rolling my own, do you have something a bit easier? ',
                answer1: 'drum',
                answer2: 'jsp',
                answer3: 'players',
                fontSize: 20
            },
        ]

        this.question1 = new question({
            scene:this.scene,
            parent:this,
            closeCallBack: this.hideQuestion,
            questionContent: this.questions[0]
        });
        this.scene.add.existing(this.question1);
        this.question2 = new question({
            scene:this.scene,
            parent:this,
            closeCallBack: this.hideQuestion,
            questionContent: this.questions[1]
        });
        this.scene.add.existing(this.question2);
        this.question3 = new question({
            scene:this.scene,
            parent:this,
            closeCallBack: this.hideQuestion,
            questionContent: this.questions[2]
        });
        this.scene.add.existing(this.question3);
        this.question4 = new question({
            scene:this.scene,
            parent:this,
            closeCallBack: this.hideQuestion,
            questionContent: this.questions[3]
        })
        this.scene.add.existing(this.question4);

        this.add([this.question1, this.question2, this.question3, this.question4]);
        this.question1.visible = false;
        this.question2.visible = false;
        this.question3.visible = false;
        this.question4.visible = false;
        this.question1.active = false;
        this.question2.active = false;
        this.question3.active = false;
        this.question4.active = false;

        
    }

    addPhone(){
        this.phone = new phone({
            scene:this.scene,
            x:0,
            y:0,
            parent:this,
            closeCallBack: this.closePhone
        });

        this.add(this.phone)
    }

    closePhone(){
        this.phone.destroy();
        this.movePeople()
    }

    movePeople(){
        let pinkwomanX;
        let yellowwomanX;
        let blueshirtX;
        let oldladyX;

        let pinkwomanDuration;
        let yellowwomanDuration;
        let blueshirtDuration;
        let oldladyDuration;

        switch (this.questionNumber) {
            case 1:
                pinkwomanX = -100;
                yellowwomanX = 90;
                blueshirtX = 280;
                oldladyX = 440;
                pinkwomanDuration = 3000;
                yellowwomanDuration = 3000;
                blueshirtDuration = 3000;
                oldladyDuration = 3000;
            break;
            case 2:
                pinkwomanX = -600;
                yellowwomanX = -100;
                blueshirtX = 90;
                oldladyX = 280;
                pinkwomanDuration = 1500;
                yellowwomanDuration = 1000;
                blueshirtDuration = 1000;
                oldladyDuration = 1000;
            break;
            case 3:
                pinkwomanX = -600;
                yellowwomanX = -600;
                blueshirtX = -100;
                oldladyX = 90;
                pinkwomanDuration = 0;
                yellowwomanDuration = 1500;
                blueshirtDuration = 1000;
                oldladyDuration = 1000;
            break;
            case 4:
                pinkwomanX = -600;
                yellowwomanX = -600;
                blueshirtX = -600;
                oldladyX = -100;
                pinkwomanDuration = 0;
                yellowwomanDuration = 0;
                blueshirtDuration = 1500;
                oldladyDuration = 1000;
            break;
            case 5:
                pinkwomanX = -600;
                yellowwomanX = -600;
                blueshirtX = -600;
                oldladyX = -600;
                pinkwomanDuration = 0;
                yellowwomanDuration = 0;
                blueshirtDuration = 0;
                oldladyDuration = 1500;
            break;
            default:
                break;
        }

        var tweenpink = this.scene.tweens.add({
            targets: this.pinkwoman,
            duration: pinkwomanDuration,
            x: pinkwomanX,
            paused: false,
            yoyo: false,
            repeat: 0
        });
        var tweenyellow = this.scene.tweens.add({
            targets: this.yellowwoman,
            duration: yellowwomanDuration,
            x: yellowwomanX,
            paused: false,
            yoyo: false,
            repeat: 0
        });
        var tweenblue = this.scene.tweens.add({
            targets: this.blueshirt,
            duration: blueshirtDuration,
            x: blueshirtX,
            paused: false,
            yoyo: false,
            repeat: 0
        });
        var tweenold = this.scene.tweens.add({
            targets: this.oldlady,
            duration: oldladyDuration,
            onStart: this.startWalkingAnimation.bind(this),
            onComplete: this.stopWalkingAnimation.bind(this),
            x: oldladyX,
            paused: false,
            yoyo: false,
            repeat: 0
        });
        
    }


    startWalkingAnimation(){
        console.log("startWalkingAnimation")
        this.pinkwoman.play('pinkwomanWalk');
        this.yellowwoman.play('yellowwomanWalk');
        this.blueshirt.play('blueshirtWalk');
        this.oldlady.play('oldladyWalk');
        
    }

    stopWalkingAnimation(){
        this.lady.play('lady');
        console.log('stopWalkingAnimation')
        this.pinkwoman.play('pinkwomanIdle');
        this.yellowwoman.play('yellowwomanIdle');
        this.blueshirt.play('blueshirtIdle');
        this.oldlady.play('oldladyIdle');
        this.showQuestion();
    }

    showQuestion(){
        switch (this.questionNumber) {
            case 1:
                this.question1.visible = true;
                this.question1.active = true;
                break;
            case 2:
                this.question2.visible = true;
                this.question2.active = true;
                break;
            case 3:
                this.question3.visible = true;
                this.question3.active = true;
                break;
            case 4:
                this.question4.visible = true;
                this.question4.active = true;
                break;
            case 4:
                this.question4.visible = true;
                this.question4.active = true;
                break;
            case 5:
                this.success = true;
                this.endGame.setText('CONGRATULATIONS!\nYou managed to clear the queue in the time.\nNow redeem your\nUnique Code...', true);
                this.endGame.visible = true;
                break;
            default:
                break;
        }

        this.questionNumber++;
    }

    hideQuestion(correct) {
        console.log(correct);
  
        if(correct) {
            this.timedEvent = this.scene.time.delayedCall(400, ()=> {
                this.question1.visible = false;
                this.question2.visible = false;
                this.question3.visible = false;
                this.question4.visible = false;
                this.question1.active = false;
                this.question2.active = false;
                this.question3.active = false;
                this.question4.active = false;
                this.movePeople();
            }, [], this);
        }else {
            this.timedEvent = this.scene.time.delayedCall(400, ()=> {
                this.question1.visible = false;
                this.question2.visible = false;
                this.question3.visible = false;
                this.question4.visible = false;
                this.question1.active = false;
                this.question2.active = false;
                this.question3.active = false;
                this.question4.active = false;
                this.success = false;
                this.endGame.setText('SORRY!\nYou did not answer all the questions correctly or you ran out of time. \nTry again to Win a\nUnique Code.', false);
                this.endGame.visible = true;
                this.resetGame();
            }, [], this);
           

        }
    }

    handleEndGame(){
        if(this.success) {
            //REDEEM CODE
        }
        else{
            //START GAME
        }
    }

    resetGame(){
        this.question1.visible = false;
        this.question2.visible = false;
        this.question3.visible = false;
        this.question4.visible = false;
        this.question1.active = false;
        this.question2.active = false;
        this.question3.active = false;
        this.question4.active = false;
        this.pinkwoman.x = 600;
        this.yellowwoman.x = 780;
        this.blueshirt.x = 930;
        this.oldlady.x = 1080;
        this.questionNumber = 1;
    }
}