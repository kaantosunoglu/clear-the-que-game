export default class question extends Phaser.GameObjects.Container {
    constructor(config){
        super(config.scene, config.x, config.y, config.children);

        this.scene = config.scene;
        this.questionContent = config.questionContent;
        this.parent = config.parent;
        this.closeCallBack = config.closeCallBack;

        this.create();
    }

    create(){
        this.setSize(980, 674);
        this.setPosition(-980/2, -674/2);
        this.bubble = this.scene.add.sprite(540, 100, 'bubble');
        this.questionText = this.scene.add.text(440, 30, this.questionContent.questionText, { fontFamily: 'raleway_bold', fontSize: this.questionContent.fontSize, color: '#000000', align: 'center', fixedWidth: 200 }).setOrigin(0,0);
        this.questionText.setWordWrapWidth(200);

        this.answer1 = this.scene.add.sprite(250, 560, this.questionContent.answer1);
        this.answer1.setInteractive();

        this.answer1.on('pointerup', function () {

            if(this.questionContent.answer1 === 'players') {
                this.correct1.visible = true;
            } else {
                this.wrong1.visible = true;
            }

                var cb = this.closeCallBack.bind(this.parent, this.questionContent.answer1 === 'players');
                cb();
            // }

        }.bind(this));

        this.answer2 = this.scene.add.sprite(500, 560, this.questionContent.answer2);
        this.answer2.setInteractive();

        this.answer2.on('pointerup', function () {
            if(this.questionContent.answer2 === 'players') {
                this.correct2.visible = true;
            } else {
                this.wrong2.visible = true;
            }
                var cb = this.closeCallBack.bind(this.parent, this.questionContent.answer2 === 'players');
                cb();
            // }

        }.bind(this));

        this.answer3 = this.scene.add.sprite(750, 560, this.questionContent.answer3);
        this.answer3.setInteractive();

        this.answer3.on('pointerup', function () {
            if(this.questionContent.answer3 === 'players') {
                this.correct3.visible = true;
            } else {
                this.wrong3.visible = true;
            }
                var cb = this.closeCallBack.bind(this.parent, this.questionContent.answer3 === 'players');
                cb();
            // }

        }.bind(this));

        this.correct1 = this.scene.add.sprite(320, 600, 'correct');
        this.wrong1 = this.scene.add.sprite(320, 600, 'wrong');
        this.correct2 = this.scene.add.sprite(570, 600, 'correct');
        this.wrong2 = this.scene.add.sprite(520, 600, 'wrong');
        this.correct3 = this.scene.add.sprite(820, 600, 'correct');
        this.wrong3 = this.scene.add.sprite(820, 600, 'wrong');

        this.correct1.visible = false;
        this.correct2.visible = false;
        this.correct3.visible = false;
        this.wrong1.visible = false;
        this.wrong2.visible = false;
        this.wrong3.visible = false;


        this.add([this.bubble, this.questionText, this.answer1, this.answer2, this.answer3, this.correct1, this.correct2, this.correct3, this.wrong1, this.wrong2, this.wrong3]);
    }
}