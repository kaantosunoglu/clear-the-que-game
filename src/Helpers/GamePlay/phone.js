export default class phone extends Phaser.GameObjects.Container {
    constructor(config){
        super(config.scene, config.x, config.y, config.children);

        this.scene = config.scene;
        this.parent = config.parent;
        this.closeCallBack = config.closeCallBack;

        this.create();
    }

    create() {
        this.setSize(980, 674);
        this.background = this.scene.add.sprite(0,0, 'phone'); 

        this.text01 = this.scene.add.sprite(10, -240, 'hp01');
        this.text02 = this.scene.add.sprite(50, -130, 'hp02');
        this.text03 = this.scene.add.sprite(80, -20, 'hp03');
        this.text04 = this.scene.add.sprite(110, 80, 'hp04');
        this.text05 = this.scene.add.sprite(130, 170, 'hp05');
        this.text06 = this.scene.add.sprite(220, 250, 'hp06');

        this.enterButton = this.scene.add.sprite(-290, 290, 'entertheshop');
        // this.enterButton.setScale(.5);
        this.enterButton.setInteractive();
        this.enterButton.on('pointerup', function () {
            var cb =  this.closeCallBack.bind(this.parent);
             cb();
         }.bind(this));
         this.text01.alpha = 0;
         this.text02.alpha = 0;
         this.text03.alpha = 0;
         this.text04.alpha = 0;
         this.text05.alpha = 0;
         this.text06.alpha = 0;
         this.enterButton.alpha = 0;
 
         
         var tween1 = this.scene.tweens.add({
             targets: this.text01,
             duration: 500,
             alpha: 1,
             paused: false,
             yoyo: false,
             repeat: 0
         });
 
         var tween2 = this.scene.tweens.add({
             targets: this.text02,
             duration: 500,
             alpha: 1,
             paused: false,
             yoyo: false,
             repeat: 0,
             delay: 1500
         });
         var tween3 = this.scene.tweens.add({
             targets: this.text03,
             duration: 500,
             alpha: 1,
             paused: false,
             yoyo: false,
             repeat: 0,
             delay: 3000
         });
         var tween4 = this.scene.tweens.add({
             targets: this.text04,
             duration: 500,
             alpha: 1,
             paused: false,
             yoyo: false,
             repeat: 0,
             delay: 4500
         });
         var tween5 = this.scene.tweens.add({
             targets: this.text05,
             duration: 500,
             alpha: 1,
             paused: false,
             yoyo: false,
             repeat: 0,
             delay: 6000
         });
         var tween6 = this.scene.tweens.add({
             targets: this.text06,
             duration: 500,
             alpha: 1,
             paused: false,
             yoyo: false,
             repeat: 0,
             delay: 7500
         });
 
         var tween7 = this.scene.tweens.add({
             targets: this.enterButton,
             duration: 500,
             alpha: 1,
             paused: false,
             yoyo: false,
             repeat: 0,
             delay: 8000
         });
 
 
     
        this.add([this.background, this.text01, this.text02, this.text03, this.text04, this.text05, this.text06, this.enterButton]);
    }



}