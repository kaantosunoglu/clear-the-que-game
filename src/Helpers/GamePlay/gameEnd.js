export default class gameEnd extends Phaser.GameObjects.Container {
    constructor(config){
        super(config.scene, config.x, config.y, config.children);

        this.scene = config.scene;
        this.questionContent = config.questionContent;
        this.parent = config.parent;
        this.closeCallBack = config.closeCallBack;

        this.create();
    }

    create() {
        this.setSize(980, 674);
        //this.setPosition(-980/2, -674/2);
        this.bubble = this.scene.add.sprite(100, 40, 'bubble2');
        
        this.questionText = this.scene.add.text(-200, -150, '', { fontFamily: 'raleway_regular', fontSize: 42, color: '#000000', align: 'center', fixedWidth: 600 }).setOrigin(0,0);
        this.questionText.setWordWrapWidth(580);
        this.add([this.bubble, this.questionText]);

        this.redeemButton = this.scene.add.sprite(290, 280, 'redeemcode');
        // this.enterButton.setScale(.5);
        this.redeemButton.setInteractive();
        this.add(this.redeemButton);
        this.redeemButton.on('pointerup', function () {
           var cb =  this.changeScreenCallBack.bind(this.parent);
            cb();
        }.bind(this));

        this.playagainButton = this.scene.add.sprite(290, 280, 'playagain');
        // this.enterButton.setScale(.5);
        this.playagainButton.setInteractive();
        this.add(this.playagainButton);


        this.playagainButton.on('pointerup', function () {
           var cb =  this.changeScreenCallBack.bind(this.parent);
            cb();
        }.bind(this));

        

    }

    setText(text, success){
        this.questionText.text = text;
        this.playagainButton.visible = !success;
        this.redeemButton.visible = success;
    }
}